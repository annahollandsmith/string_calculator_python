def add(string):
    string = normalise_delimeters(string)
    if string: 
        return add_numbers_in_string(string)
    else:
        return 0

def add_numbers_in_string(string):
    return sum(map(int, string.split(',')))

def normalise_delimeters(string):
    string = normalise_custom_delimiters(string)
    string = string.replace('\n', ',')
    return string

def normalise_custom_delimiters(string):
    if string.startswith('//'):
        delimiter = string[2]
        string = string[4:]
        string = string.replace(delimiter, ',')
    return string
