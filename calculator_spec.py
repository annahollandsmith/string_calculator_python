import unittest
from calculator import add

class TestAddStringNumbers(unittest.TestCase):
    print("Tests for `add_string_numbers` function.")

    def test_empty_string(self):
        """Test empty string as input"""
        input_ = ""
        expected = 0
        actual = add(input_)
        self.assertEqual(expected, actual)

    def test_single_string(self):
        """Test a single string with a single number as an input"""
        input_ = "1"
        expected = 1
        actual = add(input_)
        self.assertEqual(expected, actual)

    def test_single_string_returns_numerical_value(self):
        """Test a single string as an input returns appropriate numerical value"""
        input_ = "2"
        expected = 2
        actual = add(input_)
        self.assertEqual(expected, actual)

    def test_multiple_string(self):
        """Test a single string with two numbers as an input"""
        input_ = "2,3"
        expected = 5
        actual = add(input_)
        self.assertEqual(expected, actual)

    def test_string_returns_sum_of_string_numbers(self):
        """Test a single string with two numbers as an input returns the correct value"""
        input_ = "1,10"
        expected = 11
        actual = add(input_)
        self.assertEqual(expected, actual)
    
    def test_string_with_unknown_amount_of_numbers(self):
        """Test a single string with an unknown amount of numbers as an input returns the correct value"""
        input_ = "2,4,8,10"
        expected = 24
        actual = add(input_)
        self.assertEqual(expected, actual)

    def test_new_line_as_a_delimeter_between_numbers(self):
        """Test a single string with a new line delimeter"""
        input_ = "5\n8"
        expected = 13
        actual = add(input_)
        self.assertEqual(expected, actual)
    
    def test_acceptance_of_custom_delimeters(self): 
        """Test a single string with a custom delimeter"""
        input_ = "//;\n1;2"
        expected = 3
        actual = add(input_)
        self.assertEqual(expected, actual)

    def test_normal_and_custom_delimeters_used_together(self):
        """Test a single string with normal and custom delimeters"""
        input_ = "//;\n1,2;3\n4"
        expected = 10
        actual = add(input_)
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()